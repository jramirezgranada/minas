<?php

class UserTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('users')->delete();
        User::create(array(
            'name'     => 'Daniel Cardona',
            'username' => 'dcardona',
            'email'    => 'daniel@correo.com',
            'password' => Hash::make('123456'),
        ));
    }

}