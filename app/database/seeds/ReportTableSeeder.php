<?php

class ReportTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('reports')->delete();

        Report::create(array(
            'afectation'     => '1',
            'observacion' => 'essta es una observacion 1',
            'latitud'    => '86.32132',
            'longitud' => '95.6546546',
            'user_id' => 1
        ));
        Report::create(array(
            'afectation'     => '0',
            'observacion' => 'essta es una observacion 2',
            'latitud'    => '86.32132',
            'longitud' => '95.6546546',
            'user_id' => 1
        ));
        Report::create(array(
            'afectation'     => '1',
            'observacion' => 'essta es una observacion 3',
            'latitud'    => '86.32132',
            'longitud' => '95.6546546',
            'user_id' => 1
        ));
        Report::create(array(
            'afectation'     => '0',
            'observacion' => 'essta es una observacion 4',
            'latitud'    => '86.32132',
            'longitud' => '95.6546546',
            'user_id' => 1
        ));
    }

}