<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;

class ReportController extends \BaseController {


	public function __construct()
	{

	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$reports = Report::all();
		return json_encode($reports);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$rules = array(
			'afectation'    => 'required',
			'observacion' => 'required|min:5:max:50',
			'user_id' => 'required|integer'
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			$response = [
				'code' => 0,
				'message' => 'Afectation, Observacion and user_id are required'
			];
		}else{
			$data = Input::all();

			$report = new Report();
			$report->afectation = $data['afectation'];
			$report->observacion = $data['observacion'];
			$report->longitud = $data['longitud'];
			$report->latitud = $data['latitud'];
			$report->user_id = $data['user_id'];
			$report->save();

			$response = [
				'code' => 1,
				'message' => 'Report created'
			];
		}

		return json_encode($response);
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$report = Report::find($id);

		if(!$report){
			$report = [
				'code' => 0,
				'message' => 'record not found in database'
			];
		}

		return json_encode($report);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

		$rules = array(
			'afectation'    => 'required',
			'observacion' => 'required|min:5:max:50',
			'user_id' => 'required|integer'
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {
			$response = [
				'code' => 0,
				'message' => 'Afectation, Observacion and user_id are required'
			];
		}else{
			$report = Report::find($id);

			if(!$report){
				$response = [
					'code' => 2,
					'message' => 'record not found in database'
				];
			}else{
				$data = Input::all();

				$report->afectation = $data['afectation'];
				$report->observacion = $data['observacion'];
				$report->longitud = $data['longitud'];
				$report->latitud = $data['latitud'];
				$report->user_id = $data['user_id'];
				$report->save();

				$response = [
					'code' => 1,
					'message' => 'Report created'
				];
			}
		}

		return json_encode($response);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$report = Report::find($id);

		if(!$report){
			$data = [
				'code' => 0,
				'message' => 'record not found in database'
			];
		}else{
			$report->delete();

			$data = [
				'code' => 1,
				'message' => 'report deleted'
			];
		}

		return json_encode($data);

	}


}
