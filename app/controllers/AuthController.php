<?php

class AuthController extends \BaseController {

	public function doLogin()
	{

		$rules = array(
			'email'    => 'required|email',
			'password' => 'required|alphaNum|min:3'
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails()) {

			$data = [
				"code" => 2,
				"message" => "Email and password are required"
			];
		} else {

			$userdata = array(
				'email'     => Input::get('email'),
				'password'  => Input::get('password')
			);

			if (Auth::attempt($userdata)) {

				$data = [
					"code" => 1,
					"message" => "Login suscessfully"
				];

			} else {

				$data = [
					"error_code" => 0,
					"message" => "Email or password incorrect"
				];

			}

		}

		return json_encode($data);
	}


}
